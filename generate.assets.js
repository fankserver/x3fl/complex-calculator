const parser = require('fast-xml-parser');
const got = require('got');
const PropertiesReader = require('properties-reader');
const fs = require("fs");

const assetFactories = [];

(async () => {
  const factoriesXML = await got('https://raw.githubusercontent.com/kayahr/xadrian/master/src/main/resources/de/ailis/xadrian/data/x3ap/factories.xml');
  const factories = parser.parse(factoriesXML.body, {
    ignoreAttributes: false,
  });

  const messagesProperties = await got('https://raw.githubusercontent.com/kayahr/xadrian/master/src/main/resources/de/ailis/xadrian/data/x3ap/messages.properties');
  const messages = new PropertiesReader();
  messages.read(messagesProperties.body);

  const factoryIDs = {};

  for (const factory of factories.factories.factory) {
    const factoryID = factory['@_id'].split('-')
    let factoryName = messages.get('race.' + factoryID[1]) + ' ' + messages.get('factory.' + factoryID[0]);
    let tierName = '';
    if (factoryName.endsWith(' XXL')) {
      tierName = 'XXL';
      factoryName = factoryName.replace(/ XXL$/, '');
    } else if (factoryName.endsWith(' XL')) {
      tierName = 'XL';
      factoryName = factoryName.replace(/ XL$/, '');
    } else if (factoryName.endsWith(' L')) {
      tierName = 'L'
      factoryName = factoryName.replace(/ L$/, '');
    } else if (factoryName.endsWith(' M')) {
      tierName = 'M'
      factoryName = factoryName.replace(/ M$/, '');
    }

    const tier = {
      name: tierName,
      products: [
        {
          name: messages.get('ware.' + factory.product['@_ware']),
          quantity: +factory.product['@_quantity'],
          storage: +factory.product['@_storage'],
        }
      ],
      resources: [],
    };

    if (Array.isArray(factory.resource)) {
      for (const resource of factory.resource) {
        tier.resources.push({
          name: messages.get('ware.' + resource['@_ware']),
          quantity: +resource['@_quantity'],
          storage: +resource['@_storage'],
        })
      }
    } else if (factory.resource) {
      tier.resources.push({
        name: messages.get('ware.' + factory.resource['@_ware']),
        quantity: +factory.resource['@_quantity'],
        storage: +factory.resource['@_storage'],
      })
    }

    let found = false;
    for (const assetFactory of assetFactories) {
      if (assetFactory.name !== factoryName)
        continue;
      found = true;

      assetFactory.tiers.push(tier);
    }
    if (!found) {
      let factoryID = '';
      for (let i = 0; i < factoryName.length; i++) {
        if (factoryName.charAt(i) !== ' ' && factoryName.charAt(i) === factoryName.charAt(i).toUpperCase()) {
          factoryID += factoryName.charAt(i);
        }
      }
      if (factoryIDs[factoryID]) {
        for (let i = 1; i < 100; i++) {
          if (!factoryIDs[factoryID + i]) {
            factoryID = factoryID + i;
            break
          }
        }
      }
      factoryIDs[factoryID] = true;

      const assetFactory = {
        id: factoryID,
        name: factoryName,
        cycle: +factory['@_cycle'],
        tiers: [tier],
      };
      if (factoryName.includes("Solar Power")) {
        assetFactory.allowSunMultiplier = true;
      } else if (factoryName.endsWith("Mine")) {
        assetFactory.allowAsteroidYield = true;
      }
      assetFactories.push(assetFactory);
    }
  }

  for (const assetFactory of assetFactories) {
    let priority = ['', 'M', 'L', 'XL', 'XXL'];
    assetFactory.tiers.sort( function(a,b){
      if (a.name === b.name) return a.value - b.value;
      return priority.indexOf(a.name) - priority.indexOf(b.name);
    });

    let lastTierIndex = assetFactory.tiers.length-1;
    if (assetFactory.tiers[lastTierIndex].name !== priority[priority.length-1]) {
      const lastTierPriorityIndex = priority.indexOf(assetFactory.tiers[lastTierIndex].name);
      if (lastTierPriorityIndex === -1) {
        continue
      }

      for (const index in priority) {
        if (index <= lastTierPriorityIndex) {
          continue
        }

        let multiplier = 2;
        if (priority[index] === 'L') {
          multiplier = 2.5;
        }

        const products = [];
        for (const p of assetFactory.tiers[lastTierIndex].products) {
          products.push({
            name: p.name,
            quantity: p.quantity * multiplier,
            storage: p.storage * multiplier,
          });
        }

        const resources = [];
        for (const p of assetFactory.tiers[lastTierIndex].resources) {
          resources.push({
            name: p.name,
            quantity: p.quantity * multiplier,
            storage: p.storage * multiplier,
          });
        }

        assetFactory.tiers.push({
          name: priority[index],
          upgrade: true,
          products: products,
          resources: resources,
        });
        lastTierIndex++;
      }
    }
  }

  assetFactories.sort((a,  b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));

  fs.writeFileSync('src/assets/factories.json', JSON.stringify(assetFactories, null, 2));
})();

