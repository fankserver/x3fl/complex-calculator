import {Component, Inject, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {APP_BASE_HREF} from "@angular/common";
import {filter, map, share, skipUntil, startWith, take, tap, withLatestFrom} from "rxjs/operators";
import {forkJoin, iif, merge, Observable, zip} from "rxjs";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {FormControl} from "@angular/forms";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";

export interface Factory {
  id: string;
  name: string;
  cycle: number;
  tiers: FactoryTier[];
  allowSunMultiplier: boolean;
  allowAsteroidYield: boolean;
}
export interface FactoryTier {
  name: string;
  products: FactoryTierProduction[];
  resources: FactoryTierProduction[];
}
export interface FactoryTierProduction {
  name: string;
  quantity: number;
  storage: number;
}
export interface ComplexFactory {
  active: boolean;
  quantity: number;
  id: string;
  name: string;
  tier: string;
  cycle: number;
  sunPercent: number;
  yield: number;
  allowSunMultiplier: boolean;
  allowAsteroidYield: boolean;
}
export interface ComplexProduction {
  name: string;
  quantity: number;
  storage: number;
}

@Component({
  selector: 'app-builder',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.scss']
})
export class BuilderComponent {
  public factories: Factory[] = [];
  public complexFactories: MatTableDataSource<ComplexFactory> = new MatTableDataSource<ComplexFactory>([]);
  public complexFactoriesColumns: string[] = ['sorting','active','quantity','name','tier','cycle','ratio','actions'];
  public complexProduction: MatTableDataSource<ComplexProduction> = new MatTableDataSource<ComplexProduction>([]);
  public complexProductionColumns: string[] = ['name','quantity','storage'];
  public addFactoryTiers: string[] = [];
  public factoryControl = new FormControl();
  public factoryControlOptions: Observable<Factory[]>;
  public sunPercents: number[] = [0,100,150,300,400,450,600];
  public sunCycles: number[] = [305,239,215,166,145,136,114];

  constructor(
    private http: HttpClient,
    @Inject(APP_BASE_HREF) private baseHref: string,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    zip(
      this.http.get<Factory[]>(`${this.baseHref === '/' ? '' : this.baseHref}/assets/factories.json`).pipe(
        withLatestFrom(this.activatedRoute.queryParams)
      ),
    ).subscribe(([value]) => {
      this.factories = value[0];

      if (value[1].complex) {
        const complex: ComplexFactory[] = [];

        const complexParts = value[1].complex.split(';');
        for (const complexPart of complexParts) {
          const parts = complexPart.split(':');

          for (const factory of this.factories) {
            if (factory.id !== parts[0])
              continue;

            const complexFactory = {
              active: true,
              quantity: 1,
              id: factory.id,
              name: factory.name,
              tier: parts[1],
              cycle: factory.cycle,
              sunPercent: 100,
              yield: 26,
              allowAsteroidYield: factory.allowAsteroidYield,
              allowSunMultiplier: factory.allowSunMultiplier,
            };
            let indexPadding = 0;
            if (complexFactory.allowAsteroidYield) {
              complexFactory.yield = +parts[2];
              complexFactory.cycle = BuilderComponent.getMineCycle(complexFactory.yield, complexFactory.name);
              indexPadding++;
            } else if (factory.allowSunMultiplier) {
              complexFactory.sunPercent = +parts[2];
              complexFactory.cycle = this.sunCycles[this.sunPercents.indexOf(complexFactory.sunPercent)];
              indexPadding++;
            }
            if (parts.length > 2+indexPadding) {
              complexFactory.active = !!parts[2+indexPadding]
            }
            if (parts.length > 3+indexPadding) {
              complexFactory.quantity = +parts[3+indexPadding];
            }
            complex.push(complexFactory);
          }
        }

        this.complexFactories.data = complex;
        this.calculateFactoryProduction();
      }
    })
    this.factoryControlOptions = this.factoryControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  onListDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.complexFactories.data, event.previousIndex, event.currentIndex);
    this.complexFactories.data = this.complexFactories.data.slice();
    this.updateURI();
  }

  private _filter(value: string): Factory[] {
    const filterValue = value.toLowerCase();

    return this.factories.filter(factory => factory.name.toLowerCase().includes(filterValue));
  }

  factoryTiers(factoryName: string): string[] {
    const tiers: string[] = [];

    for (const factory of this.factories) {
      if (factory.name === factoryName) {
        for (const tier of factory.tiers) {
          tiers.push(tier.name);
        }
      }
    }

    return tiers;
  }

  filterAddFactoryTier(factoryName: string): void {
    this.addFactoryTiers = this.factoryTiers(factoryName);
  }

  addFactory(factoryName: string, factoryTier: string): void {
    for (const factory of this.factories) {
      if (factory.name === factoryName) {
        for (const tier of factory.tiers) {
          if (tier.name === factoryTier) {
            const complexFactory = {
              active: true,
              quantity: 1,
              id: factory.id,
              name: factory.name,
              tier: tier.name,
              cycle: factory.cycle,
              sunPercent: 100,
              yield: 26,
              allowAsteroidYield: factory.allowAsteroidYield,
              allowSunMultiplier: factory.allowSunMultiplier,
            };
            if (factory.allowAsteroidYield) {
              complexFactory.cycle = BuilderComponent.getMineCycle(complexFactory.yield, complexFactory.name)
            }
            this.complexFactories.data = this.complexFactories.data.concat(complexFactory);
          }
        }
      }
    }

    this.calculateFactoryProduction();
    this.updateURI();
  }

  updateComplexFactoryActive(index: number, active: boolean): void {
    const data = this.complexFactories.data;
    data[index].active = active;
    this.complexFactories.data = data;
    this.calculateFactoryProduction();
    this.updateURI();
  }
  updateComplexFactoryQuantity(index: number, quantity: number): void {
    const data = this.complexFactories.data;
    data[index].quantity = quantity;
    this.complexFactories.data = data;
    this.calculateFactoryProduction();
    this.updateURI();
  }
  updateComplexFactoryTier(index: number, factoryTier: string): void {
    const data = this.complexFactories.data;
    data[index].tier = factoryTier;
    this.complexFactories.data = data;
    this.calculateFactoryProduction();
    this.updateURI();
  }
  updateComplexFactorySuns(index: number, sunPercent: number): void {
    const data = this.complexFactories.data;
    data[index].sunPercent = sunPercent;
    data[index].cycle = this.sunCycles[this.sunPercents.indexOf(sunPercent)];
    this.complexFactories.data = data;
    this.calculateFactoryProduction();
    this.updateURI();
  }
  updateComplexFactoryYield(index: number, asteroidYield: number): void {
    const data = this.complexFactories.data;
    data[index].yield = asteroidYield;
    if (data[index].allowAsteroidYield) {
      data[index].cycle = BuilderComponent.getMineCycle(asteroidYield, data[index].name);
    }
    this.complexFactories.data = data;
    this.calculateFactoryProduction();
    this.updateURI();
  }
  deleteComplexFactory(index: number): void {
    const data = this.complexFactories.data;
    data.splice(index, 1);
    this.complexFactories.data = data;

    this.calculateFactoryProduction();
    this.updateURI();
  }

  calculateFactoryProduction(): void {
    const complexProducts: ComplexProduction[] = [];

    for (const complexFactory of this.complexFactories.data) {
      if (!complexFactory.active)
        continue;

      for (const factory of this.factories) {
        if (factory.name !== complexFactory.name) {
          continue
        }

        for (const tier of factory.tiers) {
          if (tier.name !== complexFactory.tier) {
            continue
          }

          for (const product of tier.products) {
            let quantity = (product.quantity / complexFactory.cycle);
            if (factory.allowAsteroidYield) {
              quantity = BuilderComponent.getMineQuantityProduct(complexFactory.yield, factory.name, tier.name);
            }
            let foundInComplex = false;
            for (const complexProduct of complexProducts) {
              if (product.name === complexProduct.name) {
                foundInComplex = true;
                complexProduct.quantity += quantity * 3600 * complexFactory.quantity;
                complexProduct.storage += product.storage * complexFactory.quantity;
              }
            }
            if (!foundInComplex) {
              complexProducts.push({
                name: product.name,
                quantity: quantity * 3600 * complexFactory.quantity,
                storage: product.storage * complexFactory.quantity,
              })
            }
          }

          for (const resource of tier.resources) {
            let quantity = (resource.quantity / complexFactory.cycle);
            if (factory.allowAsteroidYield) {
              quantity = BuilderComponent.getMineQuantityResource(complexFactory.yield, factory.name, tier.name);
            }
            let foundInComplex = false;
            for (const complexProduct of complexProducts) {
              if (resource.name === complexProduct.name) {
                foundInComplex = true;
                complexProduct.quantity -= quantity * 3600 * complexFactory.quantity;
                complexProduct.storage += resource.storage * complexFactory.quantity;
              }
            }
            if (!foundInComplex) {
              complexProducts.push({
                name: resource.name,
                quantity: -quantity * 3600 * complexFactory.quantity,
                storage: resource.storage * complexFactory.quantity,
              })
            }
          }
        }
      }
    }

    this.complexProduction.data = complexProducts.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
  }

  updateURI(): void {
    let complex = '';
    for (const complexFactory of this.complexFactories.data) {
      if (complex) {
        complex += ";"
      }
      complex += `${complexFactory.id}:${complexFactory.tier}`;
      if (complexFactory.allowAsteroidYield) {
        complex += ':' + complexFactory.yield;
      } else if (complexFactory.allowSunMultiplier) {
        complex += ':' + complexFactory.sunPercent;
      }
      complex += ':' + (complexFactory.active ? '1' : '0');
      complex += ':' + complexFactory.quantity;
    }

    const urlTree = this.router.createUrlTree([], {
      queryParams: {
        "complex": complex,
      },
      queryParamsHandling: "merge",
      preserveFragment: true,
    });
    this.router.navigateByUrl(urlTree);
  }

  private static getMineCycle(asteroidYield: number, mineName: string): number {
    const base = mineName.includes("Silicon") ? 2400 : 600;
    const basicCycleTime =  Math.floor(base / (asteroidYield + 1)) + 1;
    const multiplier = Math.floor(59.9 / basicCycleTime) + 1;
    return basicCycleTime * multiplier;
  }
  private static getMineQuantityProduct(asteroidYield: number, mineName: string, tierName: string): number {
    let basicQuantity = 2;
    if (tierName === "L") basicQuantity = 5;
    else if (tierName === "XL") basicQuantity = 10;
    else if (tierName === "XXL") basicQuantity = 20;

    const base = mineName.includes("Silicon") ? 2400 : 600;
    const basicCycleTime =  Math.floor(base / (asteroidYield + 1)) + 1;
    const multiplier = Math.floor(59.9 / basicCycleTime) + 1;
    return (multiplier * basicQuantity) / (multiplier * basicCycleTime);
  }
  private static getMineQuantityResource(asteroidYield: number, mineName: string, tierName: string): number {
    let basicQuantity = 2;
    if (tierName === "L") basicQuantity = 5;
    else if (tierName === "XL") basicQuantity = 10;
    else if (tierName === "XXL") basicQuantity = 20;

    const base = mineName.includes("Silicon") ? 2400 : 600;
    const basicCycleTime =  Math.floor(base / (asteroidYield + 1)) + 1;
    const multiplier = Math.floor(59.9 / basicCycleTime) + 1;
    return ((multiplier * basicQuantity) / (multiplier * basicCycleTime)) * (mineName.includes("Silicon") ? 24 : 6);
  }
}
