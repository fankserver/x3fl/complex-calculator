import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BuilderComponent} from "./builder/builder.component";

const routes: Routes = [
  { path: '', redirectTo: '/builder', pathMatch: 'full' },
  { path: 'builder', component: BuilderComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
